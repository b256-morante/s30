// Objective 1

db.fruits.aggregate([
        {$match: {onSale: true}},
        {$count: "fruitsOnSale"},
        {$out: "fruitsOnSale"}


])


// Objective 2

db.fruits.aggregate([
        {$match: {price: {$gte: 20}}},
        {$count: "fruitStockMoreThanOrEqualto20"},
        {$out: "fruitStockMoreThanOrEqualto20"}


])



//Objective 3

db.fruits.aggregate([
        {$match: {onSale: true}},
        {$group: {_id: "$supplier_id", minPrice: {$min: "$price"}}},
        {$out: "averagePriceOfFruitsPerSupplier"}


])

// Objective 4

db.fruits.aggregate([
        {$match: {onSale: true}},
        {$group: {_id: "$supplier_id", maxPrice: {$max: "$price"}}},
        {$out: "maxPrice"}


])

//Objective 5

db.fruits.aggregate([
        {$match: {onSale: true}},
        {$group: {_id: "$supplier_id", minPrice: {$min: "$price"}}},
        {$out: "minPrice"}


])
